﻿using System.Collections.Generic;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Hardware;
using CoffeeMakerBDD.Ingredients;

namespace CoffeeShop.Actors
{
    public abstract class Employee : IEmployee
    {
        private readonly ICoffeeMaker _coffeeMakerModel;

        protected Employee(ICoffeeMaker coffeeMakerModel)
        {
            _coffeeMakerModel = coffeeMakerModel;
        }

        protected Employee()
        {
        }

        public ICoffee Brew(List<IIngredient> ingredients)
        {
            return _coffeeMakerModel.BrewCoffee(ingredients);
        }
    }
}
