﻿using System;

namespace CoffeeMakerBDD.Exceptions
{
    public class CoffeeGroundsMissingException : Exception
    {
        public CoffeeGroundsMissingException(string message) : base(message)
        {
        }
    }
}
