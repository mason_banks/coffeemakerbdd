﻿using System.Collections.Generic;
using CoffeeMakerBDD.Ingredients;
using CoffeeMakerBDD.Rules;

namespace CoffeeMakerBDD.Coffee
{
    public class DripCoffee : ICoffee
    {
        public List<IIngredient> Ingredients { get; set; }
        public IRule QualityRule => new DripCoffeeRule();
    }
}
