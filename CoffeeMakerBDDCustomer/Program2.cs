﻿using System;
using System.Collections.Generic;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Exceptions;
using CoffeeMakerBDD.Hardware;
using CoffeeMakerBDD.Hardware.Components;
using CoffeeMakerBDD.Ingredients;
using CoffeeShop.Actors;

namespace CoffeeMakerBDDCustomer
{
    internal class Program2
    {
        static void Main2(string[] args)
        {
            introduction();
            var qualityOfEmployee = chooseEmployee();
            var carafeWaterVolume = chooseCarafeWaterVolume();
            var numberOfScoops = chooseNumberOfScoops();
            var manager = new Manager();

            try
            {
                var barista = (qualityOfEmployee.ToLower() == "good") ? chooseEmployee(true) : chooseEmployee(false);
                var coffee = customerOrder(barista, carafeWaterVolume, numberOfScoops);
                Console.WriteLine("Your barista made a " + manager.InspectCoffee(coffee).ToString().ToLower() + " cup of coffee!");
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof (FilterMissingException))
                    Console.WriteLine("The filter was not in place while brewing coffee, resulting in coffee with grounds in it.");

                if (ex.GetType() == typeof (CarafeMissingException))
                    Console.WriteLine("The carafe was not in place while brewing coffee, resulting in a big mess.");

                if (ex.GetType() == typeof(FilterOverflowException))
                    Console.WriteLine("You've exceeded the filter's maxium scoop capacity resulting in a big mess.");

                if (ex.GetType() == typeof(CarafeOverflowException))
                    Console.WriteLine("You've exceeded the carafe's maxium water capacity resulting in a big mess.");
            }

            Console.ReadLine();
            Main2(null);
        }

        private static void introduction()
        {
            Console.WriteLine("Welcome to the Coffee Shop!");
            Console.WriteLine();
            Console.WriteLine("I'm going to ask you a few questions so that I can make your coffee just the way you like it.");
            Console.WriteLine();
            Console.WriteLine();
        }

        private static string chooseEmployee()
        {
            Console.WriteLine("Tell me, would you like a 'good' employee or a 'bad' one?");
            var input = Console.ReadLine();

            if (input.ToLower() != "good" && input.ToLower() != "bad")
            {
                Console.WriteLine("You have to choose a 'good' or 'bad' employee.  You're choice is invalid");
                chooseEmployee();
            }

            return input;
        }

        private static int chooseCarafeWaterVolume()
        {
            Console.WriteLine("How many cups of water do you want in the carafe?");
            var input = Console.ReadLine();
            var cups = 0;
            try
            {
                cups = int.Parse(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("You've entered a value for carafe quantity that is not valid.  Please try again");
                chooseCarafeWaterVolume();
            }

            return cups;
        }

        private static int chooseNumberOfScoops()
        {
            Console.WriteLine("How many scoops of coffee do you want?");
            var input = Console.ReadLine();
            var scoops = 0;
            try
            {
                scoops = int.Parse(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("You've entered a value for number of scoops that is not valid.  Please try again");
                chooseCarafeWaterVolume();
            }

            return scoops;
        }

        private static IEmployee chooseEmployee(bool isGoodAtJob)
        {
            var coffeeMakerComponents = prepareCoffeeMakerComponents(isGoodAtJob);
            var preparedCoffeeMaker = prepareCoffeeMaker(coffeeMakerComponents);

            var barista = new Barista(preparedCoffeeMaker);
            return barista;
        }

        private static ICoffee customerOrder(IEmployee barista, int cups, int scoops)
        {
            return barista.Brew(prepareIngredients(cups, scoops));
        }

        private static ICoffeeMaker prepareCoffeeMaker(List<IComponent> components)
        {
            var coffeeMaker = new CoffeeMaker4Cup(components);
            return coffeeMaker;
        }

        private static List<IIngredient> prepareIngredients(int cups, int scoops)
        {
            var ingridents = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = scoops},
                new Water {Cups = cups}
            };

            return ingridents;
        }

        private static List<IComponent> prepareCoffeeMakerComponents(bool setupCorrectly)
        {
            var random = new Random();
            var randomResult = random.Next(0, 2);

            var poorlyConfiguredCoffeeMaker = (randomResult >= 1) 
                ? new List<IComponent> { new Carafe { MaxVolumeInCups = 4 } }
                : new List<IComponent> { new Filter { MaxVolumeInScoops = 4 } };

            var requiredComponents = setupCorrectly
                ? new List<IComponent> { new Carafe {MaxVolumeInCups = 4}, new Filter {MaxVolumeInScoops = 4} }
                : poorlyConfiguredCoffeeMaker;

            return requiredComponents;
        }
    }
}
