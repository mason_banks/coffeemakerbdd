﻿using System;

namespace CoffeeMakerBDD.Exceptions
{
    public class CarafeOverflowException : Exception
    {
        public CarafeOverflowException(string message) : base(message)
        {
        }
    }
}
