﻿using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Ingredients;

namespace CoffeeMakerBDD.Rules
{
    public class DripCoffeeRule : IRule
    {
        public CoffeeQuality CalculateQuality(ICoffee coffee)
        {
            var water = (Water)coffee.Ingredients.Find(x => x.GetType() == typeof(Water));
            var coffeeGrounds = (CoffeeGrounds)coffee.Ingredients.Find(x => x.GetType() == typeof(CoffeeGrounds));

            // Perfect ratio for water to coffee 
            if ((water.Cups / 2) == coffeeGrounds.Scoops)
                return CoffeeQuality.Perfect;

            // Too much water, not enough coffee
            if ((water.Cups / 2) > coffeeGrounds.Scoops)
                return CoffeeQuality.Weak;

            // Too much coffee, not enough water
            if ((water.Cups / 2) < coffeeGrounds.Scoops)
                return CoffeeQuality.Strong;

            return CoffeeQuality.Unknown;
        }
    }
}
