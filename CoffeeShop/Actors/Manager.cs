﻿using System;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Hardware;
using CoffeeMakerBDD.Rules;

namespace CoffeeShop.Actors
{
    public class Manager : Employee
    {
        public Manager(ICoffeeMaker coffeeMakerModel) : base(coffeeMakerModel) { }

        public Manager() { }

        public CoffeeQuality InspectCoffee(ICoffee coffee)
        {
            try
            {
                var ruleType = coffee.QualityRule.GetType();
                var rule = (IRule)Activator.CreateInstance(ruleType); // use reflection 
                var inspectorReport = rule.CalculateQuality(coffee);

                return inspectorReport;
            }
            catch (Exception e)
            {
                throw new Exception("There was a problem inspecting the coffee." + e);
            }
        }
    }
}
