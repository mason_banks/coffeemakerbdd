﻿using System.Collections.Generic;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Ingredients;

namespace CoffeeMakerBDD.Hardware
{
    public interface ICoffeeMaker
    {
        ICoffee BrewCoffee(List<IIngredient> ingredients);
    }
}
