﻿using System.Collections.Generic;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Ingredients;

namespace CoffeeShop.Actors
{
    public interface IEmployee
    {
        ICoffee Brew(List<IIngredient> ingredients);
    }
}
