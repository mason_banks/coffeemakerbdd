﻿using System.Collections.Generic;
using CoffeeMakerBDD.Exceptions;
using CoffeeMakerBDD.Hardware;
using CoffeeMakerBDD.Hardware.Components;
using CoffeeMakerBDD.Ingredients;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace CoffeeMakerBDDTest
{
    [TestClass]
    public class StandardCoffeeMakerTests
    {
        private const int CoffeeMakerReservoirCapacityInCups = 4;
        private const int CoffeeFilterCapacityInScoops = 4;

        private CoffeeMaker4Cup _coffeeMaker;

        [TestInitialize]
        public void Initalize()
        {
            assembleCoffeeMakerCorrectly();
        }

        [TestMethod]
        public void correct_ingredients_with_properly_configured_coffee_maker_should_make_coffee()
        {
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = CoffeeFilterCapacityInScoops / 2},
                new Water {Cups = CoffeeMakerReservoirCapacityInCups}
            };

            var coffee = _coffeeMaker.BrewCoffee(ingredients);
            coffee.ShouldNotBeNull();
        }

        [TestMethod]
        public void less_coffee_grounds_should_make_coffee()
        {
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = (CoffeeFilterCapacityInScoops - 2) / 2},
                new Water {Cups = CoffeeMakerReservoirCapacityInCups}
            };

            var coffee = _coffeeMaker.BrewCoffee(ingredients);
            coffee.ShouldNotBeNull();
        }

        [TestMethod]
        public void less_water_should_make_coffee()
        {
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = CoffeeFilterCapacityInScoops / 2},
                new Water {Cups = CoffeeMakerReservoirCapacityInCups - 2}
            };

            var coffee = _coffeeMaker.BrewCoffee(ingredients);
            coffee.ShouldNotBeNull();
        }

        [TestMethod]
        public void water_exceeding_carafe_capacity_should_overflow_carafe()
        {
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = CoffeeFilterCapacityInScoops},
                new Water {Cups = CoffeeMakerReservoirCapacityInCups + 1}
            };

            Should.Throw<CarafeOverflowException>(() =>
            {
                _coffeeMaker.BrewCoffee(ingredients);
            });
        }

        [TestMethod]
        public void coffee_grounds_exceeding_filter_capacity_should_overflow_filter()
        {
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = CoffeeFilterCapacityInScoops + 1},
                new Water {Cups = CoffeeMakerReservoirCapacityInCups}
            };

            Should.Throw<FilterOverflowException>(() =>
            {
                _coffeeMaker.BrewCoffee(ingredients);
            });
        }

        [TestMethod]
        public void no_water_should_not_brew_coffee()
        {
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = CoffeeFilterCapacityInScoops},
                new Water {Cups = 0}
            };

            Should.Throw<WaterMissingException>(() =>
            {
                _coffeeMaker.BrewCoffee(ingredients);
            });
        }

        [TestMethod]
        public void no_coffee_grounds_should_not_brew_coffee()
        {
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = 0},
                new Water {Cups = CoffeeMakerReservoirCapacityInCups}
            };

            Should.Throw<CoffeeGroundsMissingException>(() =>
            {
                _coffeeMaker.BrewCoffee(ingredients);
            });
        }

        [TestMethod]
        public void no_filter_should_not_allow_coffee_to_be_brewed()
        {
            var componentListWithoutFilter = new List<IComponent> { new Carafe() };

            Should.Throw<FilterMissingException>(() =>
            {
                var coffeeMakerWithoutFilter = new CoffeeMaker4Cup(componentListWithoutFilter);
            });
        }

        [TestMethod]
        public void no_carafe_should_not_allow_coffee_to_be_brewed()
        {
            var componentListWithoutCarafe = new List<IComponent> { new Filter() };

            Should.Throw<CarafeMissingException>(() =>
            {
                var coffeeMakerWithoutCarafe = new CoffeeMaker4Cup(componentListWithoutCarafe);
            });
        }

        private void assembleCoffeeMakerCorrectly()
        {
            var componentList = new List<IComponent> {new Filter(), new Carafe()};

            _coffeeMaker = new CoffeeMaker4Cup(componentList);
        }
    }
}
