﻿using System.Collections.Generic;
using CoffeeMakerBDD.Ingredients;
using CoffeeMakerBDD.Rules;

namespace CoffeeMakerBDD.Coffee
{
    public interface ICoffee
    {
        List<IIngredient> Ingredients { get; set; }
        IRule QualityRule { get; }
    }
}