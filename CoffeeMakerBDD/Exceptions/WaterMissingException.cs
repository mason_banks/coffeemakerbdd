﻿using System;

namespace CoffeeMakerBDD.Exceptions
{
    public class WaterMissingException : Exception
    {
        public WaterMissingException(string message) : base(message)
        {
        }
    }
}
