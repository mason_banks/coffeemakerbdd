﻿using System;

namespace CoffeeMakerBDD.Exceptions
{
    public class FilterMissingException : Exception
    {
        public FilterMissingException(string message) : base(message)
        {
        }
    }
}
