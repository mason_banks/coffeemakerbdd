﻿using System;

namespace CoffeeMakerBDD.Exceptions
{
    public class FilterOverflowException : Exception
    {
        public FilterOverflowException(string message) : base(message)
        {
        }
    }
}
