﻿using System;
using System.Collections.Generic;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Hardware;
using CoffeeMakerBDD.Hardware.Components;
using CoffeeMakerBDD.Ingredients;
using CoffeeShop.Actors;

namespace CoffeeMakerBDDCustomer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            introduction();
            Console.WriteLine("First we need to set up the 4 cup drip coffee maker...");

            var carafe = addCarafe();
            var filter = addFilter();
            var cups = addWater();
            var coffeeGrounds = addCoffee();

            Console.WriteLine("The machine is set up to your specifications... let's try to make some coffee...");
            Console.WriteLine();
            Console.WriteLine();
            System.Threading.Thread.Sleep(2000);

            var coffeeMaker = prepareCoffeeMaker(carafe, filter);

            ICoffee coffee = null;
            
            if (coffeeMaker != null)
            {
                try
                {
                    var ingredients = prepareIngredients(cups, coffeeGrounds);
                    coffee = coffeeMaker.BrewCoffee(ingredients);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("The following error was encountred while trying to brew your coffee...");
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                    Main(null);
                }


                var coffeeShopManager = new Manager();
                var coffeeQuality = coffeeShopManager.InspectCoffee(coffee);

                Console.WriteLine("You made a " + coffeeQuality.ToString().ToLower() + " cup of coffee!");
                Console.ReadLine();
            }
            else
            {
                Console.ReadLine();
            }

            Main(null);
        }

        private static void introduction()
        {
            Console.WriteLine();
            Console.WriteLine("=================================");
            Console.WriteLine();
            Console.WriteLine("Welcome to the Coffee Shop!");
            Console.WriteLine();
            Console.WriteLine("I'm going to ask you a few questions so that I can make your coffee just the way you like it.");
            Console.WriteLine();
            Console.WriteLine();
        }

        private static IComponent addCarafe()
        {
            Console.WriteLine("Would you like to put the carafe in the machine? 'y', 'n'");
            var input = Console.ReadLine();

            if (input.ToLower() != "y" && input.ToLower() != "n")
            {
                Console.WriteLine("You must type 'y' or 'n'.  Your input was invalid, let's try again...");
                addCarafe();
            }

            return input == "y" ? new Carafe() : null;
        }

        private static IComponent addFilter()
        {
            Console.WriteLine("Would you like to put the filter in the machine? 'y', 'n'");
            var input = Console.ReadLine();

            if (input.ToLower() != "y" && input.ToLower() != "n")
            {
                Console.WriteLine("You must type 'y' or 'n'.  Your input was invalid, let's try again...");
                addFilter();
            }

            return input == "y" ? new Filter() : null;
        }

        private static int addWater()
        {
            Console.WriteLine("How many cups of water do you want to put into the machine?");
            var input = Console.ReadLine();
            var cups = 0;
            try
            {
                cups = int.Parse(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("You've entered a value for carafe quantity that is not valid.  Please try again");
                addWater();
            }

            return cups;
        }

        private static int addCoffee()
        {
            Console.WriteLine("How many scoops of coffee grounds do you want to put into the machine?");
            var input = Console.ReadLine();
            var scoops = 0;
            try
            {
                scoops = int.Parse(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("You've entered a value for scoop quantity that is not valid.  Please try again");
                addWater();
            }

            return scoops;
        }

        private static ICoffeeMaker prepareCoffeeMaker(IComponent carafe, IComponent filter)
        {
            var componentList = new List<IComponent>();
            if (carafe != null) componentList.Add(carafe);
            if (filter != null) componentList.Add(filter);

            try
            {
                return new CoffeeMaker4Cup(componentList);
            }
            catch (Exception ex)
            {
                Console.WriteLine("The following error was encountered while trying to configure the coffee maker...");
                Console.WriteLine();
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        private static List<IIngredient> prepareIngredients(int water, int coffeeGrounds)
        {
            var ingredients = new List<IIngredient> {new Water {Cups = water}, new CoffeeGrounds {Scoops = coffeeGrounds} };

            return ingredients;
        }
    }
}
