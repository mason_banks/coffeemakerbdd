﻿using CoffeeMakerBDD.Hardware;

namespace CoffeeShop.Actors
{
    public class Barista : Employee
    {
        public Barista(ICoffeeMaker coffeeMakerModel) : base(coffeeMakerModel)
        {
        }
    }
}
