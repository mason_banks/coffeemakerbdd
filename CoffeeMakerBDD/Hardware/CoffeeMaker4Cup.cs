﻿using System.Collections.Generic;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Exceptions;
using CoffeeMakerBDD.Hardware.Components;
using CoffeeMakerBDD.Ingredients;

namespace CoffeeMakerBDD.Hardware
{
    public class CoffeeMaker4Cup : ICoffeeMaker
    {
        private const int CarafeMaxVolume = 4;
        private const int FilterMaxVolume = 4;

        public CoffeeMaker4Cup(List<IComponent> components)
        {
            verifyCoffeeMakerHasAllComponents(components);
        }

        public ICoffee BrewCoffee(List<IIngredient> ingredients)
        {
            verifyCoffeeMakerHasAllIngredients(ingredients);
            verifyIngredientsDoNotExceedCapacity(ingredients);

            var coffee = new DripCoffee { Ingredients = ingredients };

            return coffee;
        }

        private static void verifyCoffeeMakerHasAllComponents(List<IComponent> components)
        {
            var hasFilter = components.Exists(c => c.GetType() == typeof(Filter));
            var hasCarafe = components.Exists(c => c.GetType() == typeof(Carafe));

            if (!hasFilter)
                throw new FilterMissingException("A filter is required for this coffee maker.");

            if (!hasCarafe)
                throw new CarafeMissingException("A carafe is required for this coffee maker.");
        }

        private static void verifyCoffeeMakerHasAllIngredients(List<IIngredient> ingredients)
        {
            var water = (Water)ingredients.Find(i => i.GetType() == typeof(Water));
            var coffeeGrounds = (CoffeeGrounds)ingredients.Find(i => i.GetType() == typeof(CoffeeGrounds));

            if (water == null || water.Cups == 0)
                throw new WaterMissingException("Water is requred to make coffee with this coffee maker.");

            if (coffeeGrounds == null || coffeeGrounds.Scoops == 0)
                throw new CoffeeGroundsMissingException("Coffee grounds are requred to make coffee with this coffee maker.");
        }

        private static void verifyIngredientsDoNotExceedCapacity(List<IIngredient> ingredients)
        {
            var water = (Water)ingredients.Find(i => i.GetType() == typeof(Water));
            var coffeeGrounds = (CoffeeGrounds)ingredients.Find(i => i.GetType() == typeof(CoffeeGrounds));

            if (water.Cups > CarafeMaxVolume)
                throw new CarafeOverflowException("The carafe max capacity has been exceeded.");

            if(coffeeGrounds.Scoops > FilterMaxVolume)
                throw new FilterOverflowException("The filter max capacity has been exceeded.");
                
        }
    }
}
