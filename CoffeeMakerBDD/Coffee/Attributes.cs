﻿namespace CoffeeMakerBDD.Coffee
{
    public enum CoffeeQuality
    {
        Weak,
        Strong,
        Perfect,
        Unknown
    }
}
