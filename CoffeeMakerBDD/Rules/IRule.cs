﻿using CoffeeMakerBDD.Coffee;

namespace CoffeeMakerBDD.Rules
{
    public interface IRule
    {
        CoffeeQuality CalculateQuality(ICoffee coffee);
    }
}
