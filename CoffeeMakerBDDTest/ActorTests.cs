﻿using System.Collections.Generic;
using CoffeeMakerBDD.Coffee;
using CoffeeMakerBDD.Hardware;
using CoffeeMakerBDD.Ingredients;
using CoffeeShop.Actors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shouldly;

namespace CoffeeMakerBDDTest
{
    [TestClass]
    public class ActorTests
    {
        private Mock<ICoffeeMaker> _mockCoffeeMaker;
        private Mock<List<IIngredient>> _stubIngredients;

        [TestInitialize]
        public void Initalize()
        {
            setupIngredients();
            setupCoffeeMaker();
        }

        [TestMethod]
        public void barista_can_make_a_drip_cup_of_coffee()
        {
            var barista = new Barista(_mockCoffeeMaker.Object);
            var coffee = barista.Brew(_stubIngredients.Object);

            coffee.ShouldNotBeNull();
        }

        [TestMethod]
        public void manager_can_make_a_drip_cup_of_coffee()
        {
            var manager = new Manager(_mockCoffeeMaker.Object);
            var coffee = manager.Brew(_stubIngredients.Object);

            coffee.ShouldNotBeNull();
        }

        [TestMethod]
        public void manager_can_detect_perfect_cup_of_coffee()
        {
            var manager = new Manager();
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = 2},
                new Water {Cups = 4}
            };

            manager.InspectCoffee(new DripCoffee {Ingredients = ingredients}).ShouldBe(CoffeeQuality.Perfect);
        }

        [TestMethod]
        public void manager_can_detect_weak_cup_of_coffee()
        {
            var manager = new Manager();
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = 1},
                new Water {Cups = 4}
            };

            manager.InspectCoffee(new DripCoffee { Ingredients = ingredients }).ShouldBe(CoffeeQuality.Weak);
        }

        [TestMethod]
        public void manager_can_detect_strong_cup_of_coffee()
        {
            var manager = new Manager();
            var ingredients = new List<IIngredient>
            {
                new CoffeeGrounds {Scoops = 4},
                new Water {Cups = 4}
            };

            manager.InspectCoffee(new DripCoffee { Ingredients = ingredients }).ShouldBe(CoffeeQuality.Strong);
        }

        private void setupCoffeeMaker()
        {
            _mockCoffeeMaker = new Mock<ICoffeeMaker>();

            _mockCoffeeMaker.Setup(coffeeMaker =>
                coffeeMaker.BrewCoffee(
                    It.IsAny<List<IIngredient>>()
                )).Returns(new DripCoffee());
        }

        private void setupIngredients()
        {
            _stubIngredients = new Mock<List<IIngredient>>();
        }
    }
}
