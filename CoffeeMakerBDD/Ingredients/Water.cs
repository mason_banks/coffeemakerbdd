﻿namespace CoffeeMakerBDD.Ingredients
{
    public class Water : IIngredient
    {
        public int Cups { get; set; }
    }
}
