﻿using System;

namespace CoffeeMakerBDD.Exceptions
{
    public class CarafeMissingException : Exception
    {
        public CarafeMissingException(string message) : base(message)
        {
        }
    }
}
